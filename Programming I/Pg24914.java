/*
 problem Pg 249 #14
Janine Lee
Computer Prgm I
 */

package pg249.pkg14;

import java.util.Scanner;
public class Pg24914 {

    
public static void main(String[] args) 

{    
Scanner scan = new Scanner(System.in);
int v,r;
double r2; 
double k;   //r+h
double cost;
double height;

System.out.println("Enter the volume: ");
v= scan.nextInt();

System.out.println("Enter the radius: ");
r= scan.nextInt();

r2= r*r;
height= v/Math.PI*r2;            //formulas
k= r+height;
cost= 2*Math.PI*r*k;

System.out.println("The height is: " + height+ "\n The cost is: " + cost);

    
}
}