package binary_search_students;
import java.util.*;
import java.io.*;

/**
 Author Janine Lee
 CSCI 335 W01
 12/4/16
 **/
public class binary_search_student 
{ 
	static List oldfile= new ArrayList();	
	static int arrayid[] = new int[10];
	static Map newfile = new HashMap();

	    public static void main(String[] args) throws IOException
	    {	
	    	retrieve_array1();
	        int[] input = Arrays.copyOfRange(arrayid,0,10) ;
	        //old file
	        System.out.println("Old file: \n");    
	        
	        for(int i=0;i<10;i++)
	        {
	        arrayid[i]=oldfile.size()-oldfile.size()+i;
	        
	        System.out.print(oldfile.get(arrayid[i]));
	        }
	        
	        System.out.println("-----------------------\n");
	        
	        //sorted
	        new binary_search_student().sort(input);
	        //newfile
	        PrintWriter printW = new PrintWriter ("sorted_class_records.txt");
	        
	        System.out.println("New file: \n");
	      
	        for(int i=0;i<10;i++)
	        {
	        	System.out.print(newfile.get(String.valueOf(input[i])));
	        	//prints to file
	        	printW.println(newfile.get(String.valueOf(input[i])));
	        }
	        printW.close();
	    }
	    
	    public void sort(int array[]) 
	    {
	        for (int i = 1; i < array.length; i++) {
	               int x = array[i];
	               int y = Math.abs(Arrays.binarySearch(array, 0, i, x) + 1);

	               System.arraycopy(array, y, array, y+1, i-y);
	               array[y] = x;
	        }
	    }
	    
	    public static void retrieve_array1() throws IOException
	    {
	    	//retrieves id only, to be later sorted based on number
	    	BufferedReader br = new BufferedReader(new FileReader("class_records.txt"));
	    	String line;
	    	int i=0;
	        while ((line = br.readLine()) != null) 
	        {
	           String[]a=line.split("\\, ");
	           int id= Integer.parseInt(a[0]);

	           arrayid[i++]= id;
	           
	           oldfile.add(line + "\n");
	           
	           newfile.put(String.valueOf(id), line +"\n"); 
	           
	        } 
	        br.close();
	        
	    }
	    
		int binarySearch(int a[], int id, int lowid, int highid)
		{
			int midid = (lowid + highid)/2;
		    
			if (highid <= lowid)
		        return (id > a[lowid])? (lowid + 1): lowid;
		 
		    if(id == a[midid])
		        return midid+1;
		 
		    if(id > a[midid])
		        return binarySearch(a, id, midid+1, highid);
		    return binarySearch(a, id, lowid, midid-1);
		}
		 
		void insertSort(int a[], int n)
		{
		    int i, loc, x, select;
		 
		    for (i = 1; i < n; ++i)
		    {
		        x = i - 1;
		        select = a[i];
		        loc = binarySearch(a, select, 0, x);
	
		        while (x >= loc)
		        {
		            a[x+1] = a[x];
		            x--;
		        }
		        a[x+1] = select;
		    }
		}
}
